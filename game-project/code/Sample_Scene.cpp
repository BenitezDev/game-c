/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Sample_Scene.hpp"
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

#include <rapidxml.hpp>
using namespace rapidxml;


using namespace basics;
using namespace std;

namespace example
{

    Sample_Scene::Sample_Scene()
    {
        canvas_width  = 1280;
        canvas_height =  720;
    }

    bool Sample_Scene::initialize ()
    {
        state     = LOADING;
        suspended = false;
        x         = 640;
        y         = 360;

        return true;
    }

    void Sample_Scene::suspend ()
    {
        suspended = true;
    }

    void Sample_Scene::resume ()
    {
        suspended = false;
    }

    void Sample_Scene::handle (Event & event)
    {
        if (state == RUNNING)
        {
            switch (event.id)
            {
                case ID(touch-started):
                case ID(touch-moved):
                case ID(touch-ended):
                {
                    x = *event[ID(x)].as< var::Float > ();
                    y = *event[ID(y)].as< var::Float > ();
                    break;
                }
            }
        }
    }

    void Sample_Scene::update (float time)
    {
        switch (state)
        {
            case LOADING: load ();     break;
            case RUNNING: run  (time); break;
        }
    }

    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended && state == RUNNING)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear        ();
                canvas->set_color    (1, 1, 1);
                canvas->draw_point   ({ 360, 360 });
                canvas->draw_segment ({   0,   0 }, { 1280, 720 });
                canvas->draw_segment ({   0, 720 }, { 1280,   0 });

                if (texture)
                {
                    canvas->fill_rectangle ({ x, y }, { 100, 100 }, texture.get ());
                }
            }
        }
    }

    void Sample_Scene::load ()
    {
        if (!suspended)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                texture = Texture_2D::create (ID(test), context, "test.png");

                if (texture)
                {
                    context->add (texture);

                    state = RUNNING;
                }
            }
        }
    }

    void Sample_Scene::run (float )
    {
    }


    // prueba de XML
    void load_xml(const std::string & xml_file_pathconst, std::string & xml_file_path)
    {
        // Se intenta abrir el archivo especificado para leer su contenido:

        fstream xml_file(xml_file_path, fstream::in);

        if(xml_file.good())
        {
            // Se carga en un vector el contenido del archivo XML:

            vector< char > xml_content;

            bool finished = false;

            do
            {
                int character = xml_file.get ();

                if (character != -1)
                {
                    xml_content.push_back ((char)character);
                }
                else
                    finished = true;
            }
            while (!finished);

            xml_content.push_back (0);

            // Se parsea el archivo XML cargado en el vector:

            xml_document< > document;

            document.parse< 0 > (xml_content.data ());

            // Se obtiene un puntero al primer nodo del documento XML:

            xml_node< > * node = document.first_node ();

            // Si no hay nodo raiz implica que ha ocurrido algun error al parsear:

            if (node == NULL)
            {
               // ERROR
            }

        }
    }

}
