Game developed in C++ using Android Studio.

-> Engine done by Ángel Rodríguez Ballesteros
	Copyright © 2018+ Ángel Rodríguez Ballesteros
	Distributed under the Boost Software License, version  1.0

-> Implementation of the game by Alejandro Benítez López
	Distributed under license CC BY-NC 3.0